//
//  VCTestSplitViewController.h
//  SearchBug
//
//  Created by Vladimír Čalfa on 27.4.2013.
//  Copyright (c) 2013 Vladimír Čalfa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCMyClass.h"

@interface VCTestSplitViewController : UISplitViewController

@property (nonatomic, strong) VCMyClass *myClass;

@end
