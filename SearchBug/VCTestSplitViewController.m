//
//  VCTestSplitViewController.m
//  SearchBug
//
//  Created by Vladimír Čalfa on 27.4.2013.
//  Copyright (c) 2013 Vladimír Čalfa. All rights reserved.
//

#import "VCTestSplitViewController.h"

@interface VCTestSplitViewController ()

@end

@implementation VCTestSplitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // On iOS 6.0 and above:
    // My bug appear on second line (no. 54)
    // Bug can I fix changing propetry splitViewController from week to strong in VCMyClass
    // or move this two lines into init method.
    // The controller was only initialized and view itself wasn't loaded before dealloc. But is necessarily call loadView and load view when  UISplitViewController is deallocing?
    // In my scenario UISplitViewController is in UITabBarController
    // UISplitViewController can be only initialized when it was not showing.
    // On iOS 5.1 is this code OK because loadView isn't never call on deallocing UISplitViewController
    self.myClass = [[VCMyClass alloc] init];
    self.myClass.splitViewController = self;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
