//
//  VCViewController.m
//  SearchBug
//
//  Created by Vladimír Čalfa on 27.4.2013.
//  Copyright (c) 2013 Vladimír Čalfa. All rights reserved.
//

#import "VCViewController.h"

@interface VCViewController ()

@end

@implementation VCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)initViewControllers:(id)sender {
    
    self.testSplitViewController = [[VCTestSplitViewController alloc] init];
    self.testViewController = [[VCTestViewController alloc] init];
}

- (IBAction)deallocViewControllers:(id)sender {
    self.testSplitViewController = Nil;
    self.testViewController = Nil;
}
@end
