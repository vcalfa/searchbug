//
//  VCViewController.h
//  SearchBug
//
//  Created by Vladimír Čalfa on 27.4.2013.
//  Copyright (c) 2013 Vladimír Čalfa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCTestSplitViewController.h"
#import "VCTestViewController.h"

@interface VCViewController : UIViewController

@property (nonatomic, strong) VCTestSplitViewController *testSplitViewController;
@property (nonatomic, strong) VCTestViewController *testViewController;

- (IBAction)initViewControllers:(id)sender;
- (IBAction)deallocViewControllers:(id)sender;

@end
