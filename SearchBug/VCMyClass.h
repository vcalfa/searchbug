//
//  VCSomeClass.h
//  SearchBug
//
//  Created by Vladimír Čalfa on 27.4.2013.
//  Copyright (c) 2013 Vladimír Čalfa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VCMyClass : NSObject

@property (nonatomic, weak) UISplitViewController *splitViewController;

@end
