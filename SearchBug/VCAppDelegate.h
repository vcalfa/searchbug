//
//  VCAppDelegate.h
//  SearchBug
//
//  Created by Vladimír Čalfa on 27.4.2013.
//  Copyright (c) 2013 Vladimír Čalfa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
